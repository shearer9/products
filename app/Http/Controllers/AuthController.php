<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->only('logout');
    }

    public function register(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6'
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'password' => Hash::make($request['password'])
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    public function login(Request $request)
    {
        $this->validate(request(), [
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6'
        ]);

        if (auth()->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
            $user = auth()->user();
            $token = $user->createToken('user-token');
            $response = [
                'success' => true,
                'token' => $token->plainTextToken
            ];
            return $response;
        }

        return response()->json([
            'error' => 'Unauthenticated user',
            'code' => 401,
        ], 401);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        return response()->json('success');
    }
}
