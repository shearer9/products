<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        return Order::with('user','product')->get();
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'product_id' => 'required|integer|exists:products,id',
            'amount' => 'required|integer',
        ]);
        $user = auth()->user();
        $product = Product::find($request->product_id);
        $order = $user->products()->attach($product->id, ['amount' => $request->amount]);
        $product->stock += $request->amount;
        $product->save();
        return response()->json(['message' => 'success']);
    }
}
