<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return Product::all();
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer'
        ]);

        $product = Product::create(request([
            'name',
            'description',
            'price'
        ]));

        return $product;
    }

    public function show(Product $product)
    {
        return $product;
    }

    public function update(Request $request, Product $product)
    {
        $this->validate(request(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer'
        ]);

        $product->update(request([
            'name',
            'description',
            'price'
        ]));

        return $product;
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response('Success', 200);
    }
}
