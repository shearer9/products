<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('logout', 'AuthController@logout');

Route::apiResource('products', 'ProductController', ['middleware' => 'auth:sanctum']);
Route::middleware('auth:sanctum')->post('/orders', 'OrderController@store');
Route::middleware('auth:sanctum')->get('/orders', 'OrderController@index');
